#!/usr/bin/python
# -*- coding: utf-8 -*-

#  Imports
import json
import sys
import requests
import smtplib
import datetime
from time import sleep
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def sendMail(error):
    fromaddr = "mymineralert@gmail.com"
    toaddr = "nicolasalbani@gmail.com"
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "Miner Alert"

    msg.attach(MIMEText(error, 'plain'))

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(fromaddr, "019713Skull")
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()
    return;

# Constantes
miner_id = '02ca7f'
url = 'http://%s.ethosdistro.com/?json=yes' % miner_id
total_rig_count = 1
check_period = 60  # in seconds

#  Variables
con = None

while 1 == 1:
    try:
        # GET de status de miner
        get_result = requests.get(url)

        if get_result.status_code != 200:
            print 'GET failed!'
            sys.exit(1)

        # Processing result
        alive_gpus = (json.loads(get_result.content)['alive_gpus'])
        total_gpus = (json.loads(get_result.content)['total_gpus'])
        alive_rigs = (json.loads(get_result.content)['alive_rigs'])
        capacity = (json.loads(get_result.content)['capacity']).encode('utf-8')

        # Checking for errors
        error = ""
        if total_gpus != alive_gpus:
            error += "- Total gpu count is {0} but only {1} are alive!\n".format(total_gpus, alive_gpus)
        if alive_rigs != total_rig_count:
            error += "- Total rig count is {0} but only {1} are alive!\n".format(total_rig_count, alive_rigs)
        if capacity != "100.0":
            error += "- Total capacity is {0}!\n".format(capacity)

        # Sending alert in case of error
        if error != "":
            print("%s error(s) found, sending e-mail" % datetime.datetime.now())
            sendMail(error)
        else:
            print("%s miners ok" % datetime.datetime.now())

        # Sleeping before next check
        sleep(check_period)

    except requests.RequestException, e:
        print "Error %s:" % e.args[0]
        sys.exit(1)

    finally:
        if con:
            con.close()

