#!/usr/bin/python
# -*- coding: utf-8 -*-

#  Imports
import sys
import requests
import csv

# Constantes
url = 'https://min-api.cryptocompare.com/data/all/coinlist'
output_file = 'coinlist.csv'

#  Variables
con = None

try:
    # Validating input
    if len(sys.argv) <= 1:
        print("# USAGE: python list_coins.py <coin_list>")
        sys.exit(1)

    # GET de status de miner
    get_result = requests.get(url)

    if get_result.status_code != 200:
        print 'GET failed!'
        sys.exit(1)

    # Getting result data
    data = get_result.json()['Data']

    # Writing to csv file
    with open(output_file, 'wb') as csvfile:
        coin_writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

        # Writing csv header
        coin_writer.writerow(['Name', 'Symbol', 'CoinName', 'Algorithm', 'ProofType'])

        # Processing all coins
        for x in range(1, len(sys.argv)):
            # We use uppercase convention for coin symbol
            coin = sys.argv[x].upper()

            # Checking if coin is contained inside data
            if coin not in data:
                print("Coin %s not found!") % coin
            else:
                # Writing coin data to file
                coin_data = data[coin]
                coin_writer.writerow([coin_data['Name'], coin_data['Symbol'], coin_data['CoinName'], coin_data['Algorithm'], coin_data['ProofType']])

except requests.RequestException, e:
    print "Error %s:" % e.args[0]
    sys.exit(1)

finally:
    if con:
        con.close()

