# List Coins

List coins will create a csv file containing technical details of the provided list of coins. The list should contain the coin symbol (separated by space).

## Dependencies

> apt-get update

> apt-get install python

> apt-get install python-pip

> export LC_ALL=C

> pip install --upgrade pip

> pip install requests

## Running script

> python list_coins.py <coin_list>

e.g. python list_coins.py btc eth xrp